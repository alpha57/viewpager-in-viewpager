package xyz.ridsoft.viewpagertest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.ridsoft.viewpagertest.R;

/**
 * Created by RiD on 2018. 3. 29..
 */

public class Fragment1 extends Fragment {

    private ViewPager mPager;
    private FragmentPagerAdapter mAdapter;
    private Fragment[] mFragments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_1, container, false);

        initData();

        mPager = view.findViewById(R.id.vp_sub);

        /*android.support.v4.app.Fragment 를 extends 하는 클래스에서 getFragmentManager()를 호출하면
            FragmentPagerAdapter 의 인자로 전달할 수 있는 FragmentManager 를 리턴받을 수 있습니다. */
        mAdapter = new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (mFragments != null && position < mFragments.length) {
                    return mFragments[position];
                } else {
                    return null;
                }
            }

            @Override
            public int getCount() {
                if (mFragments != null) {
                    return mFragments.length;
                } else {
                    return 0;
                }
            }
        };

        //TODO: 페이지가 바뀌었을때 listener
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //페이지가 스크롤 되었을 때
                //이 곳에서 페이지 인디케이터를 표시하거나 업데이트 합니다.
            }

            @Override
            public void onPageSelected(int position) {
                //페이지가 선택되었을 때 (??)
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //스크롤 상태가 바뀌었을 때
            }
        });

        //ViewPager 의 adapter 로 FragmentPagerAdapter 를 설정합니다.
        mPager.setAdapter(mAdapter);

        return view;
    }

    private void initData() {
        mFragments = new Fragment[2];
        mFragments[0] = new SubFragment1();
        mFragments[1] = new SubFragment2();
    }

}
