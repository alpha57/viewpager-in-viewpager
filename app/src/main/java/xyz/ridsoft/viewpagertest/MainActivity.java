package xyz.ridsoft.viewpagertest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import xyz.ridsoft.viewpagertest.fragments.Fragment1;
import xyz.ridsoft.viewpagertest.fragments.Fragment2;

public class MainActivity extends AppCompatActivity {

    private ViewPager mPager;
    private FragmentPagerAdapter mAdapter;
    private Fragment[] mFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

        mPager = findViewById(R.id.vp_main);

        //TODO: FragmentPagerAdapter 의 인자로 android.support.v4.app.Fragment 를 받습니다.
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (mFragments != null && position < mFragments.length) {
                    return mFragments[position];
                } else {
                    return null;
                }
            }

            @Override
            public int getCount() {
                if (mFragments != null) {
                    return mFragments.length;
                } else {
                    return 0;
                }
            }
        };

        //TODO: 페이지가 바뀌었을때 listener
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //페이지가 스크롤 되었을 때
                //이 곳에서 페이지 인디케이터를 표시하거나 업데이트 합니다.
            }

            @Override
            public void onPageSelected(int position) {
                //페이지가 선택되었을 때 (??)
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //스크롤 상태가 바뀌었을 때
            }
        });

        //ViewPager 의 adapter 로 FragmentPagerAdapter 를 설정합니다.
        mPager.setAdapter(mAdapter);

    }

    private void initData() {
        mFragments = new Fragment[2];
        mFragments[0] = new Fragment1();
        mFragments[1] = new Fragment2();
    }
}
